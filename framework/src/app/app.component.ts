import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  imageList = ["../assets/image/first-page.png", "../assets/image/learn.png",
              "../assets/image/first-page.png", "../assets/image/learn.png",
              "../assets/image/first-page.png", "../assets/image/learn.png",
              "../assets/image/first-page.png", "../assets/image/learn.png",
              "../assets/image/first-page.png", "../assets/image/learn.png"]
}
