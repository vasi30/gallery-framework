import { Component, Input } from '@angular/core';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent {
  @Input() images: string[];
  selectedImage: string;

  changeSelectedImage(image){
    this.selectedImage = image;
  }

  toggle(){

  }
}
