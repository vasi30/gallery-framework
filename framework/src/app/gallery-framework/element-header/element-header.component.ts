import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'element-header',
    templateUrl: './element-header.component.html',
    styleUrls: ['./element-header.component.css']
})
export class ElementHeaderComponent implements OnInit {
    @Input() position: string = 'left'

    ngOnInit() {
    }

    getPosition() {
        if (this.position && this.position != 'left' && this.position != 'right'){
            console.log(this.position);
            return { "left" : 0};
        }
        var first = this.position;
        var result = {}
        result[first] = 0;
        return result
    }
}
