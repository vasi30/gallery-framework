import { Component, Input } from '@angular/core';

@Component({
  selector: 'image-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  @Input() images: string[];
  selectedImage: string;

  changeSelectedImage(image){
    this.selectedImage = image;
  }
}
