import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GalleryComponent } from './gallery-framework/gallery.component'
import { PageHeaderComponent } from './gallery-framework/page-header/page-header.component'
import { ElementHeaderComponent } from './gallery-framework/element-header/element-header.component'
import { ElementFooterComponent } from './gallery-framework/element-footer/element-footer.component'

@NgModule({
  declarations: [
    AppComponent,
    GalleryComponent,
    PageHeaderComponent,
    ElementHeaderComponent,
    ElementFooterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
